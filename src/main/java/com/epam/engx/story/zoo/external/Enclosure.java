package com.epam.engx.story.zoo.external;

public interface Enclosure {

    Animal getAnimal();

    String getId();
}
