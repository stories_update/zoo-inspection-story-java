package com.epam.engx.story.zoo.external;

public interface EnclosureStatus {

    boolean isEnclosureSafe();
}
