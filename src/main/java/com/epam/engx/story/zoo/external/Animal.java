package com.epam.engx.story.zoo.external;

public interface Animal {

    String getName();

}
